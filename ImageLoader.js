/*
    ImageLoader v.1.1
*/
var ImageLoader = {};

ImageLoader.init = function(params) {   
    ImageLoader.images = params.images;
    ImageLoader.imageCount = Object.keys(ImageLoader.images).length;
    ImageLoader.callback = params.callback;
    ImageLoader.load();
}

ImageLoader.load = function() {
    for (var i in ImageLoader.images) {
        ImageLoader.images[i].image = new Image();
        ImageLoader.images[i].image.onload = function() {
            ImageLoader.imageCount--;
            ImageLoader.checkIfLoaded();
        }
        ImageLoader.images[i].image.src = ImageLoader.images[i].src;
    }
}

ImageLoader.checkIfLoaded = function() {        
    if (ImageLoader.imageCount == 0) ImageLoader.callback();
}

export default ImageLoader;