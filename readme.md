# imageloader

## Usage

```javascript
import ImageLoader from './js/ImageLoader';

ImageLoader.init({
    images: {
        'test': {
            src: 'https://cdn.vox-cdn.com/uploads/chorus_asset/file/9826547/giphy__2_.gif'
        }
    },
    callback: function () {        
        document.getElementById('myElement').style.backgroundImage = 'url(' + ImageLoader.images['test'].src + ')';
    }
});
```